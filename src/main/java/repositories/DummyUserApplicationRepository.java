package repositories;

import domain.UserApplication;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by saviola on 3/19/16.
 */
public class DummyUserApplicationRepository implements UserApplicationRepository {

    private static List<UserApplication> db = new ArrayList<UserApplication>();

    public UserApplication getApplicationByUsername(String username) {
       for (UserApplication application: db){
           if(application.getUsername().equalsIgnoreCase(username))
               return application;
       }
        return null;
    }

    public void add(UserApplication application) {
        db.add(application);
    }

    public int count() {
        return db.size();
    }
}
