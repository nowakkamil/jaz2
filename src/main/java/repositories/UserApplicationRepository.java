package repositories;

import domain.UserApplication;

/**
 * Created by saviola on 3/19/16.
 */
public interface UserApplicationRepository {

    UserApplication getApplicationByUsername(String username);
    void add(UserApplication application);
    int count();

}
