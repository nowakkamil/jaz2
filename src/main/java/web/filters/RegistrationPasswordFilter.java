package web.filters;

import repositories.DummyUserApplicationRepository;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import java.io.IOException;

/**
 * Created by saviola on 3/20/16.
 */
@WebFilter({"/", "/register"})
public class RegistrationPasswordFilter implements Filter {

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
        throws IOException, ServletException{

        if(request.getParameter("password").equals(request.getParameter("confirmpassword"))) {
            chain.doFilter(request, response);
        }
        else
        {
            response.getWriter().print("Hasla nie sa identyczne");
            return;
        }

    }

    @Override
    public void destroy() {

    }

}
