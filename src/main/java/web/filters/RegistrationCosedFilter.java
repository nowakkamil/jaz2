package web.filters;

import repositories.*;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import java.io.IOException;

/**
 * Created by saviola on 3/20/16.
 */
@WebFilter({"/", "/register"})
public class RegistrationCosedFilter implements Filter {

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
        throws IOException, ServletException{

        if(new DummyUserApplicationRepository().getApplicationByUsername((String) request.getParameter("username"))!=null){
            response.getWriter().print("Istnieje juz uzytkownik o takim loginie.");
            return;
        }
        chain.doFilter(request, response);
    }

    @Override
    public void destroy() {

    }

}
