//package test.java;
//
//import domain.ConferenceApplication;
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.mockito.InjectMocks;
//import org.mockito.Mockito;
//import org.mockito.Spy;
//import org.mockito.runners.MockitoJUnitRunner;
//import repositories.UserApplicationRepository;
//import web.RegisterUserServlet;
//
//import javax.servlet.ServletException;
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import javax.servlet.http.HttpSession;
//import java.io.IOException;
//
///**
// * Created by saviola on 3/20/16.
// */
//@RunWith(MockitoJUnitRunner.class)
//public class TestAddApplicantServlet extends Mockito {
//    @Spy
//    UserApplicationRepository repository = mock(UserApplicationRepository.class);
//
//    @InjectMocks
//    RegisterUserServlet servlet;
//
//    @Test
//    public void servlet_should_write_info_about_applicant_into_session()
//        throws IOException, ServletException{
//        HttpServletRequest request = mock(HttpServletRequest.class);
//        HttpServletResponse response = mock(HttpServletResponse.class);
//        HttpSession session = mock(HttpSession.class);
//        when(request.getSession()).thenReturn(session);
//        servlet.doPost(request,response);
//        verify(session).setAttribute(eq("conf"), Mockito.any(ConferenceApplication.class));
//
//    }
//
//
//}
